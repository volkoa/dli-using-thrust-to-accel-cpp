#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <iostream>

int main(void)
{
  // allocate some device vectors
  thrust::device_vector<int> vecA(100);
  thrust::device_vector<int> vecB;

  vecB.resize(1 << 30);
  vecA[100] = 50;

  thrust::sort(vecB.begin(), vecB.end());
  thrust::copy(vecB.begin(), vecB.end(), vecA.begin());
  
  std::cout << "Finished running program" << std::endl;

  return 0;
}