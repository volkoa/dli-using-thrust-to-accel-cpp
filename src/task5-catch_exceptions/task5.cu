/*
 * Compile/run: nvcc -O2 -arch=sm_30 task5/task5.cu -run
 */
#include <thrust/random.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/system_error.h>

#include <thrust/random.h>

#include <iostream>
#include <sys/time.h>


typedef long long unsigned int UINT64;

// https://stackoverflow.com/questions/12614164/generating-random-numbers-with-uniform-distribution-using-thrust
struct GenRand {
    __host__  __device__
    int operator ()(int idx) {
        UINT64 seed = clock() + idx;
        thrust::default_random_engine rng(seed);
        thrust::uniform_real_distribution<float> uniDist(0, 1.0);
        // rng.discard(idx);
        rng.discard(seed);
        // int num = 100 * uniDist(rng);
        // printf("%f", num);
        return 100 * uniDist(rng);
    }
};

int main(void) {
    const int N = 100;
    // allocate some device vectors
    thrust::device_vector<int> vecA(N);
    // thrust::host_vector<int> vecA(N);
    thrust::device_vector<int> vecB;

    //const int randNum = 1 << 30; // Size of vector - FIXME
    const int randNum = N; // Size of vector

    try {
        vecB.resize(randNum);
    } catch (std::bad_alloc &e) {
        std::cerr << "Couldn't allocate vecB" << std::endl;
        exit(-1);
    }

    try {
        // vecA[100] = 50; // FIXME
        vecA[99] = 50;
        std::cout << "Last element of vecA set to: " << vecA[99] << std::endl;
    } catch (thrust::system_error &e) {
        std::cerr << "Error occured during assignment to vecA: " << e.what()
            << std::endl;
        exit(-1);
    }

    try {
        //thrust::transform(
        //    thrust::make_counting_iterator(0),
        //    thrust::make_counting_iterator(N),
        //    vecB.begin(),
        //    GenRand());
        thrust::tabulate(vecB.begin(), vecB.end(), GenRand());

        std::cout << "Vector B pre-sorted:" << std::endl;
        thrust::copy(vecB.begin(), vecB.begin() + N,
            std::ostream_iterator<int>(std::cout, ","));
        std::cout << std::endl;

        thrust::sort(thrust::device, vecB.begin(), vecB.end());
        // thrust::copy(vecB.begin(), vecB.end(), vecA.begin()); // FIXME
        thrust::copy(vecB.begin(), vecB.begin() + N - 1, vecA.begin());

        std::cout << "Vector A:" << std::endl;
        thrust::copy(vecA.begin(), vecA.end(),
            std::ostream_iterator<int>(std::cout, ","));
        std::cout << std::endl;

    } catch (std::bad_alloc &e) {
        std::cerr << "Ran out of memory" << std::endl;

        // either exit or handle the error condition
    } catch (thrust::system_error &e) {
        // output an error message
        std::cerr << "Error: " << e.what() << std::endl;

        // either exit or handle the error condition
    }

    std::cout << "Finished running program" << std::endl;

    return 0;
}
