/*
 * Compile/run: nvcc -O2 -arch=sm_30 task7/task7.cu -run
 */
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/reduce.h>
#include <thrust/random.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/transform.h>
#include <thrust/tabulate.h>

#include <vector_functions.hpp> // make_float2

#include <vector>
#include <cstdlib>
#include <iostream>

// // https://stackoverflow.com/questions/6150368/cuda-and-eclipse-how-can-i-tell-eclipse-that-or-is-part-of-the-syntax
// #if (defined __CDT_PARSER__) || (defined __INTELLISENSE__) || (defined Q_CREATOR_RUN)
// // this stuff is defined in:
// // #include <crt/host_defines.h>
// #define __global__
// #define __device__
// #define __host__
// #define __shared__
// #endif

typedef long long unsigned int UINT64;

// TODO: annotate this function with __host__ __device__ so
//       so that it is able to work with Thrust
// float2 operator+(float2 a, float2 b) {
// Defining an "+" operator for float2 so it would automatically be used in reduce.
__host__ __device__
float2 operator+(float2 a, float2 b) {
    return make_float2(a.x + b.x, a.y + b.y);
}

// https://github.com/jaredhoberock/thrust-workshop/blob/dcf73c94ada5a7b022e278eea79984fabf9c2e00/fun_with_points/performance.cu
struct GenRand {
    __host__  __device__
    float2 operator ()(int idx) {
        UINT64 seed = clock() + idx;
        thrust::default_random_engine rng(seed);
        thrust::uniform_real_distribution<float> uniDist;
        rng.discard(idx);
        float x = uniDist(rng);
        float y = uniDist(rng);
        // printf("%f", x);
        // printf("%f", y);
        return make_float2(x, y);
    }
};

// void generate_random_points(std::vector<float2> &points) {
void generate_random_points(thrust::device_vector<float2> &points) {
    // sequentially generate some random 2D points in the unit square
    //std::cout << "TODO: parallelize this loop using thrust::tabulate\n"
    //    << std::endl;
    //
    //for (int i = 0; i < points.size(); ++i) {
    //    float x = float(rand()) / RAND_MAX;
    //    float y = float(rand()) / RAND_MAX;
    //
    //    points[i] = make_float2(x, y);
    //}

    // https://thrust.github.io/doc/group__transformations.html#ga0ddf108cc4d1109addeed3251824f951
    //void tabulate(ForwardIterator first,
    //              ForwardIterator last,
    //              UnaryOperation unary_op)
    thrust::tabulate(points.begin(), points.end(), GenRand());

}

//float2 compute_centroid(const std::vector<float2> &points) {
float2 compute_centroid(const thrust::device_vector<float2> &points) {
    float2 init = make_float2(0, 0);

    // compute the sum
    //std::cout << "TODO: parallelize this sum using thrust::reduce\n"
    //    << std::endl;
    //for (int i = 0; i < points.size(); ++i) {
    //    sum = sum + points[i];
    //}

    // https://thrust.github.io/doc/group__reductions.html#ga6ac0fe1561f58692e85112bd1145ddff
    float2 sum = thrust::reduce(points.begin(), points.end(), init);
    // divide the sum by the number of points
    return make_float2(sum.x / points.size(), sum.y / points.size());
}

struct CalcQuadrant {
    float2 centroid_;
    CalcQuadrant(float2 centroid) : centroid_(centroid) {};

    __host__  __device__
    int operator()(const float2 & p) const {
        return (p.x <= centroid_.x ? 0 : 1) | (p.y <= centroid_.y ? 0 : 2);
    }
};

//void classify_points_by_quadrant(const std::vector<float2> &points,
//    float2 centroid, std::vector<int> &quadrants) {
void classify_points_by_quadrant(const thrust::device_vector<float2> &points,
    float2 centroid, thrust::device_vector<int> &quadrants) {
    // classify each point relative to the centroid
    //std::cout << "TODO: parallelize this loop using thrust::transform\n"
    //    << std::endl;
    //for (int i = 0; i < points.size(); ++i) {
    //    float x = points[i].x;
    //    float y = points[i].y;
    //
    //    // bottom-left:  0
    //    // bottom-right: 1
    //    // top-left:     2
    //    // top-right:    3
    //
    //    quadrants[i] = (x <= centroid.x ? 0 : 1) | (y <= centroid.y ? 0 : 2);
    //}

    thrust::transform(
        points.begin(), points.end(), quadrants.begin(),
        CalcQuadrant(centroid));

}

//void count_points_in_quadrants(std::vector<float2> &points,
//    std::vector<int> &quadrants, std::vector<int> &counts_per_quadrant) {
void count_points_in_quadrants(thrust::device_vector<float2> &points,
    thrust::device_vector<int> &quadrants,
    thrust::device_vector<int> &counts_per_quadrant) {
    // sequentially compute a histogram
    //std::cout << "TODO: parallelize this loop by" << std::endl;
    //std::cout << "   1. sorting points by quadrant" << std::endl;
    //std::cout << "   2. reducing points by quadrant\n" << std::endl;
    //for (int i = 0; i < quadrants.size(); ++i) {
    //    int q = quadrants[i];
    //
    //    // increment the number of points in this quadrant
    //    counts_per_quadrant[q]++;
    //}

    // quadrants are keys

    // https://thrust.github.io/doc/group__sorting.html#ga2bb765aeef19f6a04ca8b8ba11efff24
    thrust::sort_by_key(
        quadrants.begin(), quadrants.end(),
        points.begin());

    // https://thrust.github.io/doc/group__reductions.html#ga1fd25c0e5e4cc0a6ab0dcb1f7f13a2ad
    //    const int N = 7;
    //    int A[N] = {1, 3, 3, 3, 2, 2, 1}; // input keys
    //    int B[N] = {9, 8, 7, 6, 5, 4, 3}; // input values
    //    int C[N];                         // output keys
    //    int D[N];                         // output values
    //    thrust::reduce_by_key(A, A + N, B, C, D);
    //    // The first four keys in C are now {1, 3, 2, 1}
    //    // The first four values in D are now {9, 21, 9, 3}
    // That's why need to pre-sort to get reduce to work on consecutive nums.

    //thrust::pair<OutputIterator1,OutputIterator2> thrust::reduce_by_key (
    //    InputIterator1  keys_first, InputIterator1  keys_last,
    //    InputIterator2  values_first,
    //    OutputIterator1     keys_output,
    //    OutputIterator2     values_output)

    // count points in each quadrant
    thrust::reduce_by_key(
        quadrants.begin(), quadrants.end(), // keys_first, keys_last
        thrust::constant_iterator<int>(1), // all values are 1 i.e. ++ increment
        thrust::discard_iterator<>(), // don't care about keys output i.e. squashed quadrants vector
        counts_per_quadrant.begin() // index into counts_per_quadrant corresponds to quadrant.
    );
}

std::ostream &operator<<(std::ostream &os, float2 p) {
    return os << "(" << p.x << ", " << p.y << ")";
}

int main() {
    const size_t num_points = 10000000;

    //std::cout
    //    << "TODO: move these points to the GPU using thrust::device_vector\n"
    //    << std::endl;
    //std::vector<float2> points(num_points);

    thrust::device_vector<float2> points(num_points);

    generate_random_points(points);

    float2 centroid = compute_centroid(points);

    //std::cout
    //    << "TODO: move these quadrants to the GPU using thrust::device_vector\n"
    //    << std::endl;
    //std::vector<int> quadrants(points.size());
    thrust::device_vector<int> quadrants(points.size());
    classify_points_by_quadrant(points, centroid, quadrants);

    //std::cout
    //    << "TODO: move these counts to the GPU using thrust::device_vector\n"
    //    << std::endl;
    //std::vector<int> counts_per_quadrant(4);
    thrust::device_vector<int> counts_per_quadrant(4);
    count_points_in_quadrants(points, quadrants, counts_per_quadrant);

    std::cout << "Per-quadrant counts:" << std::endl;
    std::cout << "  Bottom-left : " << counts_per_quadrant[0] << " points"
        << std::endl;
    std::cout << "  Bottom-right: " << counts_per_quadrant[1] << " points"
        << std::endl;
    std::cout << "  Top-left    : " << counts_per_quadrant[2] << " points"
        << std::endl;
    std::cout << "  Top-right   : " << counts_per_quadrant[3] << " points"
        << std::endl;
    std::cout << std::endl;

//    int expected_count_per_quadrant = num_points / 4;
//    int tolerance = expected_count_per_quadrant / 1000;
//    for (int i = 0; i < 4; ++i) {
//        assert(abs(expected_count_per_quadrant - counts_per_quadrant[i])
//                < tolerance);
//    }

    return 0;
}
