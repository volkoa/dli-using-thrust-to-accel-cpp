/*
 * Compile/run:
 *     nvcc -O2 -arch=sm_30 -o task4_out task4/task4.cu -run
 *     nvcc -O2 -arch=sm_30 -o task4_cpu task4/task4_solution.cu -Xcompiler -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp
 *
 * Profile: nvprof ./task4_cpu
 *
 */
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
#include <thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/replace.h>
#include <thrust/functional.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <iostream>

void constantIterator() {
    std::cout << "Constant Iterator:\n";

    thrust::constant_iterator<int> first(5);
    // thrust::constant_iterator<int> last = #FIXME
    // http://thrust.github.io/doc/classthrust_1_1constant__iterator.html
    thrust::constant_iterator<int> last = first + 4;

    // sum of [first, last)
    // int value = #FIXME
    // // http://thrust.github.io/doc/group__reductions.html#ga69434d74f2e6117040fb38d1a28016c2
    int value = thrust::reduce(first, last);

    std::cout << "  value = " << value << ". Should be 20\n\n";
}

void transformIterator() {
    std::cout << "Transform Iterator:\n";

    thrust::device_vector<int> d_vec(3);
    thrust::sequence(d_vec.begin(), d_vec.end(), 10, 10); // d_vec[0] = 10, d_vec[1] = 20, ...

    // sum of [first, last)
    //int value = thrust::reduce(thrust::make_transform_iterator(#FIXME),
    //                         thrust::make_transform_iterator(#FIXME));
    // http://thrust.github.io/doc/classthrust_1_1transform__iterator.html
    // http://thrust.github.io/doc/structthrust_1_1negate.html
    int value = thrust::reduce(
        thrust::make_transform_iterator(d_vec.begin(), thrust::negate<int>()),
        thrust::make_transform_iterator(d_vec.end(), thrust::negate<int>()));

    std::cout << "  value = " << value << ". Should be -60\n\n";
}

void zipIterator() {
    std::cout << "Zip Iterator:\n";

    thrust::device_vector<int> A(4);
    thrust::device_vector<char> B(4);
    A[0] = 10; A[1] = 20; A[2] = 30; A[3] = 30;
    B[0] = 'x'; B[1] = 'y'; B[2] = 'z'; B[3] = 'a';
    //int arr[4] = { 10, 20, 30, 30 };
    //thrust::device_vector<int> A(arr, arr + 4);
    //char arr2[4] = { 'x', 'y', 'z', 'a' };
    //thrust::device_vector<char> B(arr2, arr2 + 4);

    // maximum of [first, last)
    thrust::maximum<thrust::tuple<int, char> > binary_op;

    thrust::tuple<int, char> init;
    // Speficy the initial value for the maximize reduction
    // This is the lowest possible value we could have
    init = thrust::make_tuple(-1 * INT_MAX, 'a');

    thrust::tuple<int, char> value;
    //value = thrust::reduce(#FIXME,
    //                     #FIXME,
    //                     init,
    //                     binary_op);
    auto ab_tuple_beg_iter = thrust::make_zip_iterator(
        thrust::make_tuple(A.begin(), B.begin()));
    auto ab_tuple_end_iter = thrust::make_zip_iterator(
        thrust::make_tuple(A.end(), B.end()));
    value = thrust::reduce(ab_tuple_beg_iter, ab_tuple_end_iter, init,
        binary_op);

    std::cout << "  value = (" << value.get<0>() << "," << value.get<1>()
        << "). Should be (30,z)\n\n";
}

// No need to modify the main() function
int main(void) {
    constantIterator();

    transformIterator();

    zipIterator();

    return 0;
}
