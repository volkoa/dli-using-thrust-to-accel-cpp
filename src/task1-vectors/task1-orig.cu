#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/copy.h>

#include "../../src/task1-vectors/timer.h"

int main()
{
  thrust::host_vector<int> h_vec( 50000000 );
  
  thrust::generate( h_vec.begin(), h_vec.end(), rand );
  
  StartTimer();
  
  #FIXME Move the data to the device, sort it, and move it back
  
  double runtime = GetTimer();
  printf("Runtime: %f s\n", runtime / 1000);

  return 0;
}