/*
 * Compile/run: nvcc -O2 -arch=sm_30 task2/task2.cu -run
 */
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
#include <thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/replace.h>
#include <thrust/functional.h>
#include <iostream>

int main(void) {
    // Allocate three device_vectors with 10 elements
    thrust::device_vector<int> X(10);
    thrust::device_vector<int> Y(10);
    thrust::device_vector<int> Z(10);

    // Initialize X to 0,1,2,3,... using thrust::sequence
    // #FIXME
    // http://thrust.github.io/doc/group__transformations.html#ga08c1dd7914c155d5fed6458330af3443
    thrust::sequence(X.begin(), X.end(), 0, 1);

    // Fill Z with twos with thrust::fill
    // #FIXME
    // http://thrust.github.io/doc/group__filling.html#ga95479e3d2f1af2ca19aedf7fd9db0465

    thrust::fill(Z.begin(), Z.end(), 2);

    // Compute Y = X mod 2 with thrust::transform and thrust::modulus functor
    // #FIXME
    // http://thrust.github.io/doc/group__transformations.html#ga68a3ba7d332887f1332ca3bc04453792
    // http://thrust.github.io/doc/structthrust_1_1modulus.html
    thrust::transform(X.begin(), X.end(), Z.begin(), Y.begin(),
        thrust::modulus<int>());

    // At this point, Y should contain either 0's for odd indexes or 1's for even indexes
    // Replace all the ones in Y with tens with thrust::replace
    // #FIXME
    // http://thrust.github.io/doc/group__replacing.html#gaf4c7616600c8937aa31c73417cfb4f28
    thrust::replace(Y.begin(), Y.end(), 1, 10);

    // Print Y using the thrust::copy and the following functor:
    // std::ostream_iterator<int>(std::cout, "\n")
    // #FIXME
    // http://thrust.github.io/doc/group__copying.html#ga24ccfaaa706a9163ec5117758fdb71b9
    thrust::copy(Y.begin(), Y.end(), std::ostream_iterator<int>(std::cout, "\n"));

    return 0;
}
