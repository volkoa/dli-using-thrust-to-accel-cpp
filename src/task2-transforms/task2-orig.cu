#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
#include <thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/replace.h>
#include <thrust/functional.h>
#include <iostream>

int main(void)
{
  // Allocate three device_vectors with 10 elements
  thrust::device_vector<int> X(10);
  thrust::device_vector<int> Y(10);
  thrust::device_vector<int> Z(10);

  // Initialize X to 0,1,2,3,... using thrust::sequence
  #FIXME

  // Fill Z with twos with thrust::fill
  #FIXME

  // Compute Y = X mod 2 with thrust::transform and thrust::modulus functor
  #FIXME

  // At this point, Y should contain either 0's for odd indexes or 1's for even indexes
  // Replace all the ones in Y with tens with thrust::replace
  #FIXME

  // Print Y using the thrust::copy and the following functor:
  // std::ostream_iterator<int>(std::cout, "\n")
  #FIXME
   
  return 0;    
}