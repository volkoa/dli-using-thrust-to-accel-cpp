/*
 * Compile/run: nvcc -O2 -arch=sm_30 task6/task6.cu -run
 *
 * Produce output:
 *     Before transform.
 *     After transform.
 *     terminate called after throwing an instance of 'thrust::system::system_error'
 *       what():  device free failed: an illegal memory access was encountered
 *     Aborted
 *
 * To DEBUG compile/run: nvcc -DTHRUST_DEBUG -O2 -arch=sm_30 task6/task6.cu -run
 */
#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/functional.h>
#include <iostream>

int main(void) {
    thrust::device_vector<int> x(1000);

    std::cerr << "Before transform." << std::endl;

    // transform into a bogus location
    thrust::transform(x.begin(), x.end(), thrust::device_pointer_cast<int>(0),
        thrust::negate<int>());

    std::cerr << "After transform." << std::endl;

    return 0;
}
