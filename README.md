This is a port of the DLI/Course for Using Thrust to Accelerate C++.<br/>
<https://courses.nvidia.com/courses/course-v1:DLI+L-AC-18+V1/about>

Follow along with the instructions below or via python notebook
[Cpp_Thrust.ipynb](src/Cpp_Thrust.ipynb).


# Using Thrust to Accelerate C++

In this self-paced, hands-on lab, we will learn how to use the Thrust parallel
programming library to accelerate C++ code on GPUs and CPUs.

Lab created by Mark Ebersole (Follow [@CUDAHamster](https://twitter.com/@cudahamster)
on Twitter), with examples and text borrowed heavily from the
[Thrust website](http://thrust.github.io/) created by Jared Hoberock.

---

## Introduction to Thrust

Thrust is a parallel algorithms library loosely based on the C++ Standard
Template Library. Thrust provides a number of building blocks, such as sort,
scans, transforms, and reductions, to enable developers to quickly embrace the
power of parallel computing. In addition to targeting the massive parallelism
of NVIDIA GPUs, Thrust supports multiple system back-ends such as OpenMP and
Intel’s Threading Building Blocks. This means that it’s possible to compile your
code for different parallel processors with a simple flick of a compiler switch.

This lab consists of four tasks that will require you to modify code, compile
and execute it. For each task, a solution is provided so you can check your
work or take a peek if you get lost. It is expected to take a C++ programmer
new to Thrust about 90 minutes to complete the tasks in his lab.

### Task #1

The goal of your first task is to get you warmed up writing code in Thrust. If
you have ever used the C++ Standard Template Library (STL) before, this task
should be fairly straight forward. The code in Task #1 will move some randomly
generated data to the GPU, sort it, and then copy it back to the host. Before
we jump into the code, let's go over the basics of working with Thrust. 

First off, to help avoid naming conflicts, C++ makes use of namepsaces and
Thrust is no exception. In this lab and in most of the Thrust code you will see,
all the Thrust functions and members will be preceded by `thrust::` to indicate
which namespace it comes from. In this lab, you will also see reference to
the `std::` namespace for printing out values to the screen.

#### Containers

Whereas the STL has many different types of containers, Thrust just works with
two vector types:

* Host vectors are declared with `thrust::host_vector<type>`
* Device vectors are declared with `thrust::device_vector<type>`

When declaring a host or device vector, you must provide the data type it will
contain. In fact, since Thrust is a template library, most of your declarations
will involve specifying a type. These types can be common simple native
data-types like `int`, `char`, or `float`. But the type can also be complex
structures like a `thrust::tuple` which contains multiple elements. For details
on how to initialize a host or device vector, I encourage you to look at the
Thrust documentation [here](http://thrust.github.io/doc/group__container__classes.html).
For this lab, the two methods needed to initialize a Thrust vector are the following:

* Create a host or device vector of a specific size:
`thrust::host_vector<type> h_vec( SIZE );` or `thrust::device_vector<type> d_vec( SIZE );`

 * It's common practice to precede host vector variables with `h_` and device
 vector variables with `d_` to make it clear in the code which memory space
 they are referring to.

* Create and initialize a device vector from an existing Thrust vector:
`thrust::device_vector<type> d_vec = h_vec;`

 * Under the covers, Thrust will handle allocating space on the device that is
 the same size as `h_vec`, as well as copying the memory from the host to the device.
 
#### Iterators

Now that we have containers for our data in Thrust, we need a way for our
algorithms to access this data, regardless of what type of data they contain.
This is where C++ iterators come in to play. In the case of vector containers,
which are really just arrays, iterators can be thought of as pointers to array
elements. Therefore, `H.begin()` is an iterator that points to the first
element of the array stored inside the H vector. Similarly, `H.end()` points to
the element one past the last element of the H vector.

Although vector iterators are similar to pointers, they carry more information
with them. We do not have to tell Thrust algorithms that they are operating on
a device_vector or host_vector iterator. This information is captured in the
type of the iterator returned by `H.begin()`. When a Thrust function is called,
it inspects the type of the iterator to determine whether to use a host or a
device implementation. This process is known as static dispatching since the
host/device dispatch is resolved at compile time. Note that this implies that
there is **no runtime overhead** to the dispatch process.

#### Functions

With containers and iterators, we can finally process our data using functions.
Almost all Thrust functions process the data by using iterators pointing at
different vectors. For example, to copy data from a device vector to a host
vector, the following code is used:

`thrust::copy( d_vec.begin(), d_vec.end(), h_vec.begin() );`

This function simply states "Starting at the first element of `d_vec`, copy the
data starting at the beginning of `h_vec`, advancing through each vector until
the end of `d_vec` is reached".

#### Task Instructions

Your objective in this task is to replace the `#FIXME` of
[task1.cu](src/task1-vectors/task1-orig.cu) with code that does the following:

1. Create a device_vector and copy the initialized `h_vec` data to it using
the `=` operator as discussed above

2. Sort the data on the device with [`thrust::sort`](http://thrust.github.io/doc/group__sorting.html#ga01621fff7b6eb24fb68944cc2f10af6a)

3. Move the data back to `h_vec` using [`thrust::copy`](http://thrust.github.io/doc/group__copying.html#ga24ccfaaa706a9163ec5117758fdb71b9)

The solution to this task is provided in
[task1_solution.cu](src/task1-vectors/task1_solution.cu). Please look at it to
check your work, or if you get stuck.

To compile and run, simply execute the cell below.

```bash
# Execute this cell to compile task1.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task1-vectors/task1.cu -run
```

Congrats! You have successfully executed code on the GPU using Thrust, and you
did not have to write any GPU specific code! As we'll see in [Task #4](#task4),
with just a compiler switch, you can compile Thrust code to execute on a CPU.

### Task #2

Most of the Thrust functions are intended to be building blocks, allowing you
the programmer to build complex algorithms on top of them. The purpose of this
task is to give you more experience using Thrust functions and iterators, and
to expose you to additional functions available. 

In addition, you will also start working with "functors" in this task. A functor
is a “function object”, which is an object that can be called as if it were an
ordinary function. In C++ a functor is just a class or struct that defines the
function call operator. Because they are objects, functors can be passed (along
with their state) to other functions as a parameter. Thrust comes with a handful
of pre-defined functors, one of which we'll use in this task. In the next task,
we'll see how to write your own functor and use it in a Thrust algorithm.

There are a few ways to use a functor. One is to create it like you would a
normal object like this:

    thrust::modulus<float> modulusFunctor(...); // Create the functor, if required, pass in any arguments to the constructor.
    float result = modulusFunctor(4.0, 2.0); // Use the functor just like a regular function
    ...

The second method is to call the constructor directly in an argument list to another function:

    thrust::transform(..., thrust::modulus<float>() );

You'll notice we have to add the `()` after `<float>` as we're calling the
functors constructor to instantiate the function object. The Thrust `transform`
function can now apply the functor to all the elements it's working with.

Open up [task2.cu](src/task2-transforms/task2-orig.cu) as before. Your
objective is to replace the `#FIXME` sections of code to achieve the following.
Note that each item links to the relevant Thrust documentation.

1. Initialize the `X` vector with 0,1,2,3,...,9 using
[`thrust::sequence`](http://thrust.github.io/doc/group__transformations.html#ga08c1dd7914c155d5fed6458330af3443)

2. Fill the `Z` vector with all 2's using
[`thrust::fill`](http://thrust.github.io/doc/group__filling.html#ga95479e3d2f1af2ca19aedf7fd9db0465)

3. Set `Y` equal to `X mod Z` using
[`thrust::transform`](http://thrust.github.io/doc/group__transformations.html#ga68a3ba7d332887f1332ca3bc04453792)
and [`thrust::modulus`](http://thrust.github.io/doc/structthrust_1_1modulus.html)

4. Replace all the 1's in `Y` with 10's with
[`thrust::replace`](http://thrust.github.io/doc/group__replacing.html#gaf4c7616600c8937aa31c73417cfb4f28)

5. Print out the result of `Y` with
[`thrust::copy`](http://thrust.github.io/doc/group__copying.html#ga24ccfaaa706a9163ec5117758fdb71b9)
and copying it to the output iterator `std::ostream_iterator<int>(std::cout, "\n")`

To make sure you are getting the correct answer, the program prints out the `Y`
device vector. If everything was done correctly, you should see the following output:
<pre>
0
10
0
10
0
10
0
10
0
10
</pre>

The cells to compile and execute the program are located below. If you get
stuck, there are a number of hints provided.

Finally, you can open up [task2_solution.cu](src/task2-transforms/task2_solution.cu)
to check your work, or to look at if you feel lost.

***Hint #1***

If you use the `thrust::modulus<int>` constructor directly in the `thrust::transform`
argument list, don't forget to add the `()` to call the constructor.
      
***Hint #2***

The last parameter to `thrust::copy` to print out the `Y` vector should be
`std::ostream_iterator<int>(std::cout, "\n")`. This says that as each element
is copied to the `std::ostream_iterator`, print it to `std::cout` and append a
newline to it.


```bash
# Execute this cell to compile task2.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task2-transforms/task2.cu -run
```

If you are still not able to get the correct output, please have a look at the
[task2_solution.cu](src/task2-transforms/task2_solution.cu) file and see if you
can figure out what you were missing!

### Task #3

Thrust provides a few built-in functors for you to use, but the real power
comes from creating your own functors. For this task, we will remove the call
to `thrust::replace` from the code in Task #2, and instead replace that
functionality with a custom functor used in the `thrust::transform` call. An
example of a custom functor is the following unary functor which returns the
square of the input value:
<pre>
template &lt;typename T&gt;
struct square
{
  __host__ __device__
  T operator()(const T& x) const
  { 
    return x * x;
  }
};
</pre>

The `__host__ __device__` line above tells the nvcc compiler to compile both a
Host and a Device version of the function below it. This maintains portability
between CPUs and GPUs.

The way this functor works is we're overriding the `()` operator of the struct.
This is the most versatile of the overloadable operators, as it can accept any
number and type of inputs, and return any type of output. In this way, the
Thrust algorithms simply has to call `outputType = someFunctor( inputType1, inputType2, ..., inputTypeN )`
without having to have an understanding of what the function does. This makes
for a very powerful and flexible library!

**Note:** It is not necessary to make your custom functor a template struct,
but it adds a lot of flexibility to your code.

In [task3.cu](src/task3-functors/task3-orig.cu), finish creating the
`modZeroOrTen` functor, and then call it from the `thrust::transform` function.
If everything is done right, you should get the same output as Task #2, which is:
<pre>
0
10
0
10
0
10
0
10
0
10
</pre>

Solution file: [task3_solution.cu](src/task3-functors/task3_solution.cu)


***Hint #1***

The custom functor for our code needs to be a binary operator - it takes two
values as input. The `square` functor example shown is only a unary operator.
      
***Hint #2***

If you are creating the `square` functor object directly in the `thrust::transform`
argument list, don't forget to add the `()` to call the constructor.
      
***Hint #3***

Don't forget to add, at a minimum, the `__device__` keyword before your
function so the compiler knows to compile this function for the GPU.


```bash
# Execute this cell to compile task3.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task3-functors/task3.cu -run 
```

By creating this custom functor, we were able to eliminate the `thrust::replace`
call, which makes for a more efficient application.

### Task #4

So far, we've only dealt with basic iterators that allow Thrust to cycle
through all the elements in a vector. Fancy iterators perform a variety of
valuable purposes. In this task we'll show how fancy iterators allow us to
attack a broader class of problems with the standard Thrust algorithms. While
we won't cover all the fancy iterators in this task, we'll cover three of them.

The simplest of the bunch, `constant_iterator` is simply an iterator that
returns the same value whenever we dereference it. In the following example we
initialize a `constant_iterator` with the value 10.
<pre>
// create iterators
thrust::constant_iterator<int> first(10);
thrust::constant_iterator<int> last = first + 3; // Set the last element to be 3 past the beginning

// sum of [first, last)
thrust::reduce(first, last);   // returns 30 (i.e. 10 + 10 + 10)
</pre>

The `transform_iterator` allows us to apply the technique of combining separate
algorithms, without having to rely on Thrust to provide a special `transform_xxx`
version of the algorithm. This task shows another way to fuse a transformation
with a reduction, this time with just plain reduce applied to a `transform_iterator`.

The following example prints out all the elements in the `values` vector, after
clamping them between 0 and 100.
<pre>
thrust::copy(thrust::make_transform_iterator(values.begin(), clamp<int>(0, 100)), // First element
             thrust::make_transform_iterator(values.end(), clamp<int>(0, 100)), // End element
             std::ostream_iterator<int>(std::cout, " "));
</pre>

Finally, the `zip_iterator` is an **extremely** useful gadget: it takes multiple
input sequences and yields a sequence of tuples. The following example applies
the `arbitrary_functor` to each tuple, where each tuple is made up of elements
from `A`, `B`, `C`, and `D` vectors. You can see details on the
`thrust::for_each` function [here](http://thrust.github.io/doc/group__modifying.html#ga263741e1287daa9edbac8d56c95070ba).

<pre>
thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(A.begin(), B.begin(), C.begin(), D.begin())),
                 thrust::make_zip_iterator(thrust::make_tuple(A.end(),   B.end(),   C.end(),   D.end())),
                 arbitrary_functor());
</pre>

One downside of `transform_iterator` and `zip_iterator` is that it can be
cumbersome to specify the full type of the iterator, which can be quite lengthy.
For this reason, it is common practice to simply put the call to
`make_transform_iterator` or `make_zip_iterator` in the arguments of the
algorithm being invoked.

Your objective in this task is to modify [task4.cu](src/task4-fancy_iters/task4-orig.cu)
and write the code to implement each type of iterator. The different iterator
types are broken up into three functions - there is no need to modify the
`main()` function. If you want, you can comment out the inside of the functions
you have yet to implement while you focus on one. The goal for each is to
replace the `#FIXME` sections:

1. For the constant_iterator, use
[`thrust::reduce`](http://thrust.github.io/doc/group__reductions.html#ga69434d74f2e6117040fb38d1a28016c2)
and [`thrust::constant_iterator`](http://thrust.github.io/doc/classthrust_1_1constant__iterator.html)
to print out the value 20.

2. For the transform_iterator, use the
[`thrust::make_transform_iterator`](http://thrust.github.io/doc/classthrust_1_1transform__iterator.html)
and [`thrust::negate`](http://thrust.github.io/doc/structthrust_1_1negate.html)
functor to print out -60.

3. For the zip_iterator, use `thrust::make_tuple` and `thrust::make_zip_iterator`
to return the maximum element of the tuple made up of (A, B).

Solution file: [task4_solution.cu](src/task4-fancy_iters/task4_solution.cu)

```bash
# Execute this cell to compile task4.cu and if successful, run the program
nvcc -O2 -arch=sm_30 -o task4_out task4-fancy_iters/task4.cu -run
```

### <a name="task4"></a> Run on the CPU

The below is an example of how to compile the solution from task #4 to execute
on a multi-core CPU using Thrust's OpenMP backend. Without modifying any code,
we can change what the device vector in our Thrust code is targeting!

The `-Xcompiler` command tells nvcc to apply the following options to the host
compiler, in our case, that's gcc. **Note:** It's possible to compile Thrust
code with the non-CUDA backend using a standard host compiler.


```python
# Execute this cell to compile task4.cu for the CPU
nvcc -O2 -arch=sm_30 -o task4_cpu task4-fancy_iters/task4_solution.cu -Xcompiler -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp
```

To prove that the compiled task4_cpu is indeed not running on the GPU, we can
profile it with NVIDIA's command-line GPU profiler `nvprof`. After executing
the above cell to compiler the code with an OpenMP backend, execute the below
cell. You should see `Warning: No CUDA application was profiled, exiting` in
the output, indicating that nothing was run on the GPU.

If you'd like to profile the GPU version, simply change `./task4_cpu` to
`./task4_out` after successfully compiling the code from Task #4.

```bash
nvprof ./task4_cpu
```

### Task #5

For the final task in our lab, we'll see how to handle and interpret errors
from Thrust calls. As with most programming, it's very important to check for
errors in your code.

#### Catching Exceptions

When a Thrust function detects an error condition resulting from a lower-level
API, it throws an exception to communicate the error to the caller. In Thrust,
exceptions are usually instances of `thrust::system_error` objects, which
inherits from `std::runtime_error`. You can catch these exceptions in your code
and try to handle the error as appropriate. Below is the syntax for catching
general system errors in Thrust:

```
  try
  {
    // Some code
  }
  catch(thrust::system_error &e)
  {
    // output an error message
    std::cerr << "Error: " << e.what() << std::endl;
    
    // either exit or handle the error condition
  }
```

Some Thrust functions may require allocating temporary storage. If such an
allocation fails, `std::bad_alloc`, instead of `thrust::system_error`, is
returned. The syntax is as follows:

```
  try
  {
    // Some code that allocates data
  }
  catch(std::bad_alloc &e)
  {
    std::cerr << "Ran out of memory" << std::endl;
    
    // either exit or handle the error condition  
  }
```

You can also check for different error types at once.

```
  try
  {
    // Some code
  }
  catch(std::bad_alloc &e)
  {
    std::cerr << "Ran out of memory" << std::endl;
    
    // either exit or handle the error condition  
  }
  catch(thrust::system_error &e)
  {
    // output an error message
    std::cerr << "Error: " << e.what() << std::endl;
    
    // either exit or handle the error condition
  }
```

In this [code](src/task5-catch_exceptions/task5-orig.cu), add exception catching
to catch errors. You can either use the generic system error exception, or more
specific ones, or both!  First try executing the code without modification to
see what happens.

Solution file: [task5_solution.cu](src/task5-catch_exceptions/task5_solution.cu)

***Hint #1***

Don't forget to include the <code><thrust/system_error.h></code> file.


```bash
# Execute this cell to compile task5.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task5-catch_exceptions/task5.cu -run
```

You likely did not have the same exact error catching code as I did, but
hopefully you get the idea. Feel free to fix the errors in the code and make
sure it runs to completion. For additional information on handling exceptions
in C++, you look at the many tutorials available searching for "C++ exception
handling" in your favorite search engine.

#### Asynchronous Error Detection

For performance, many Thrust functions are asynchronous, which means the
execution of the function may be deferred until later. A consequence is that
errors from asynchronous functions may be reported later in the program's
execution than expected. This can make it difficult to pinpoint the source of
the error.

The code below has an asynchronous error in it. You can see what happens by
executing the cell below the code.

```
#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/functional.h>
#include <iostream>

int main(void)
{
  thrust::device_vector<int> x(1000);

  std::cerr << "Before transform." << std::endl;

  // transform into a bogus location
  thrust::transform(x.begin(), x.end(), thrust::device_pointer_cast<int>(0), thrust::negate<int>());

  std::cerr << "After transform." << std::endl;

  return 0;
}
```


```python
# Execute this cell to compile task6.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task6-asynch_error/task6.cu -run
```

You should seen something like the below:

<pre>Before transform.
After transform.
terminate called after throwing an instance of 'thrust::system::system_error'
  what():  an illegal memory access was encountered
Aborted (core dumped)</pre>

What's happening is the `thrust::transform` call is asynchronous, so execution
continues immediately after calling the function. This means we see the "After
transform" text before the error occurs. While debugging, we can make Thrust
check for errors at the earliest opportunity by enabling a debug mode by simply
defining the macro THRUST_DEBUG on the compiler's command line. This makes it
easy to find the source of the error. Try executing the below line to enable
Thrust debugging.


```bash
# Execute this cell to compile task6.cu and if successful, run the program
nvcc -DTHRUST_DEBUG -O2 -arch=sm_30 task6-asynch_error/task6.cu -run
```

## Case Study

The final exercise in this lab is to implement everything you've learned in the
following Case Study called "Fun with Points". Your task is to modify the
application to move code from the CPU to the GPU using Thrust. The outline of
what the application does is described in the image below.

<div align="center"><img src="src/images/fig1.PNG" alt="Fun With Points" style="width: 60%;"/></div>

Modify the [task7.cu](src/task7-fun_with_points/task7-orig.cu) file in the
editor below to move functions to the GPU using Thrust. The locations were you
need to use Thrust are proceeded with text that has "TODO" at the front. As
usual, if you get stuck, you can look at the
[task7_solution.cu](src/task7-fun_with_points/task7_solution.cu) file to see
the correct answer.

You can find more documentation about this example at
[its GitHub location](https://github.com/jaredhoberock/thrust-workshop/tree/master/fun_with_points).

***Hint***
In function `count_points_in_quadrants` use `thrust::sort_by_key` and `thrust::reduce_by_key`.


```bash
# Execute this cell to compile task7.cu and if successful, run the program
nvcc -O2 -arch=sm_30 task7-fun_with_points/task7.cu -run
```

## Learn More

If you are interested in learning more, you can use the following resources:

* Learn more at the [CUDA Developer Zone](https://developer.nvidia.com/Thrust).

* Thrust is provided with the [CUDA tookit](https://developer.nvidia.com/cuda-toolkit),
or you can download separately from the Thrust GitHub [site](https://github.com/thrust/thrust).

* Look for recorded sessions on Thrust at
[gputechconf.com](http://www.gputechconf.com/gtcnew/on-demand-gtc.php?searchByKeyword=thrust&searchItems=&sessionTopic=&sessionEvent=&sessionYear=&sessionFormat=&submit=&select=+).
These include talks specifically about Thrust, or projects which made use of the library.

* Search or ask questions on [Stackoverflow](http://stackoverflow.com/questions/tagged/thrust)
using the thrust tag

